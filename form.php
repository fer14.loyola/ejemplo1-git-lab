<?php
    if($_POST) {
        $nomina_foreign = $_POST['nomina'];
        $salario = $_POST['salario'];
        $faltas = $_POST['faltas'];
        $imss = $_POST['imss'];


        // Constantes
	// Otros comentario
        $folio = 0;
        $calculo_imss = 0.042;
        $isr = 0.072;
        $fon_ahorro = 0.02;
        $quincena = 15;
        $salario_total = 0;

        $val_desp = 1024;
        $ayu_comedor = 700;
        $ayu_transp = 900;

        // La suma de los tres abonos
        $total_abonos = $val_desp + $ayu_comedor + $ayu_transp;



        // Realizar las operaciones
        if($faltas >= 0 && $salario && $salario > 0) {
            $quincena -= $faltas;
            $salario_total = $quincena * $salario;

            $fon_ahorro *= $salario_total;
            $isr *= $salario_total;
            $calculo_imss *= $salario_total;

            // La suma de los tres descuentos
            $total_descuentos = $fon_ahorro + $isr + $calculo_imss;

            // Pago total - impuestos + abonos
            $pago_nomina = $salario_total + $total_abonos - $total_descuentos;
        }





        // ...
        // ...
        // ...
        // ...
        // Conexion con mysql
        $serverName = 'localhost';
        $userName = 'root';
        $password = '';
        $database = 'empleado';
        $field = 'nomina';


        $conn = mysqli_connect($serverName, $userName, $password, $database)
            or die ("No se ha podido conectar al servidor de la Base de datos");


        $insert = "INSERT INTO $field
            (folio, no_nomina, salario_diario, no_faltas, imss, isr, fondo_ahorro, vales_despensa, ayuda_comedor, ayuda_transp, salario, calculo_imss, pago_nomina)
            VALUES ($folio, $nomina_foreign, $salario, $faltas, '$imss', $isr, $fon_ahorro, $val_desp, $ayu_comedor, $ayu_transp, $salario_total, $calculo_imss, $pago_nomina)";


        // Validar se el query se ejecuto correctamente
        $resQuery = mysqli_query($conn, $insert);

        if($resQuery) {
            $newRowAdd = 'Nuevo registro añadido exitosamente';
        }
        else {
            $newRowAdd = 'No se ha podido añadir el registro';
        }


        $conn->close();
    }
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/main.css">
        <title>Formulario recibo</title>
    </head>


    <body>
        <div class="wrapper  below">
            <form
                id="send-form"
                method="POST"
                action= "<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>"
                class="form">
                <fieldset>
                    <legend>
                        <h2 class="heading4  title">
                            Datos de la nómina
                        </h2>
                    </legend>



                    <div class="form__field">
                        <label for="nomina"
                               class="img  form__icon">
                               <img src="./images/user.svg"
                                    alt="nombre"/>
                        </label>

                        <input type="tel"
                                id="nomina"
                                name="nomina"
                                maxlength="25"
                                class="input  input--small"
                                onchange="changeText(id)"/>

                        <label for="nomina"
                            class="form__text">
                            No. nómina:
                        </label>
                    </div>



                    <div class="form__first-row">
                        <!-- Primera columna -->
                        <div class="form__first-row__col">
                            <div class="form__field">
                                <label for="name"
                                       class="form__icon">
                                    <div class="img">
                                        <img src="images/user.svg"
                                             alt="nombre"/>
                                    </div>
                                </label>

                                <input type="tel"
                                        id="name"
                                        name="faltas"
                                        maxlength="25"
                                        class="input"
                                        onchange="changeText(id)"/>

                                <label for="name"
                                    class="form__text">
                                    No. faltas:
                                </label>
                            </div>



                            <div class="form__field">
                                <label for="ap_pat"
                                       class="form__icon">
                                    <div class="img">
                                        <img src="images/user.svg"
                                             alt="nombre"/>
                                    </div>
                                </label>

                                <input type="text"
                                        id="ap_pat"
                                        name="salario"
                                        maxlength="25"
                                        class="input"
                                        onchange="changeText(id)"/>

                                <label for="ap_pat"
                                    class="form__text">
                                    Salario diario:
                                </label>
                            </div>



                            <div class="form__field">
                                <label for="ap_mat"
                                       class="form__icon">
                                    <div class="img">
                                        <img src="images/user.svg"
                                             alt="nombre"/>
                                    </div>
                                </label>

                                <input type="text"
                                        id="ap_mat"
                                        name="imss"
                                        maxlength="25"
                                        class="input"
                                        onchange="changeText(id)"/>

                                <label for="ap_mat"
                                    class="form__text">
                                    IMSS:
                                </label>
                            </div>
                        </div>
                    </div>



                    <!-- Boton de enviar -->
                    <div class="form__buttton-send">
                        <input
                            type="submit"
                            name="send"
                            value="Enviar"
                            class="btn  btn--large">
                    </div>
                </fieldset>


                <a href="index.php" class="form__btn-color">
                    <div class="img  form__next">
                        <img src="./images/prev.svg" alt="">
                    </div>
                </a>

                <!-- Validacion de los registros -->
                <h3 class="heading4  subtitle  form__valid">
                    <?php if($_POST) {echo $newRowAdd;} ?>
                </h3>
            </form>




            <!-- DATOS ENVIADOS A LA BD -->
            <div class="wrapper  section  summary">
                <h2 class="heading5 subtitle">
                    Datos enviados
                </h2>


                <div class="summary__body">
                    <h3 class="summary__title-data">
                        Datos del empleado
                    </h3>
                    <div class="summary__field">
                        <p class="summary__item  summary__item--bold">Num. nómina:</p>
                        <p class="summary__item">
                            <?php if($_POST) {echo $nomina_foreign;}  else { echo " - - - -"; } ?>
                        </p>
                    </div>
                    <div class="summary__field">
                        <p class="summary__item  summary__item--bold">Num faltas:</p>
                        <p class="summary__item">
                            <?php if($_POST) {echo $faltas;}  else { echo " - - - -"; } ?>
                        </p>
                    </div>
                    <div class="summary__field">
                        <p class="summary__item  summary__item--bold">Salario diario:</p>
                        <p class="summary__item">
                            <?php if($_POST) {echo "$ ".$salario;}  else { echo " - - - -"; } ?>
                        </p>
                    </div>
                    <div class="summary__field">
                        <p class="summary__item  summary__item--bold">Num. IMSS:</p>
                        <p class="summary__item">
                            <?php if($_POST) {echo $imss;}  else { echo " - - - -"; } ?>
                        </p>
                    </div>


                    <h3 class="summary__title-data">
                        Descuentos
                    </h3>
                    <div class="summary__field">
                        <p class="summary__item  summary__item--bold">
                            ISR <span class="summary__text-small">(7.2%)</span>:
                        </p>
                        <p class="summary__item">
                            <?php if($_POST) {echo "$ ".$isr;}  else { echo " - - - -"; } ?>
                        </p>
                    </div>
                    <div class="summary__field">
                        <p class="summary__item  summary__item--bold">
                            IMSS <span class="summary__text-small">(4.2%)</span>:
                        </p>
                        <p class="summary__item">
                            <?php if($_POST) {echo "$ ".$calculo_imss;}  else { echo " - - - -"; } ?>
                        </p>
                    </div>
                    <div class="summary__field">
                        <p class="summary__item  summary__item--bold">
                            Fondo de ahorro <span class="summary__text-small">(2%)</span>:
                        </p>
                        <p class="summary__item">
                            <?php if($_POST) {echo "$ ".$fon_ahorro;}  else { echo " - - - -"; } ?>
                        </p>
                    </div>


                    <h3 class="summary__title-data">
                        Abonos
                    </h3>
                    <div class="summary__field">
                        <p class="summary__item  summary__item--bold">Vales de despensa:</p>
                        <p class="summary__item">
                            <?php if($_POST) {echo "$ ".$val_desp;}  else { echo " - - - -"; } ?>
                        </p>
                    </div>
                    <div class="summary__field">
                        <p class="summary__item  summary__item--bold">Ayuda comedor:</p>
                        <p class="summary__item">
                            <?php if($_POST) {echo "$ ".$ayu_comedor;}  else { echo " - - - -"; } ?>
                        </p>
                    </div>
                    <div class="summary__field">
                        <p class="summary__item  summary__item--bold">Ayuda trasporte:</p>
                        <p class="summary__item">
                            <?php if($_POST) {echo "$ ".$ayu_transp;}  else { echo " - - - -"; } ?>
                        </p>
                    </div>


                    <h3 class="summary__title-data">
                        Total
                    </h3>
                    <div class="summary__field">
                        <p class="summary__item  summary__item--bold">Salario:</p>
                        <p class="summary__item">
                            <?php if($_POST) {echo "$ ".$salario_total;}  else { echo " - - - -"; } ?>
                        </p>
                    </div>
                    <div class="summary__field">
                        <p class="summary__item  summary__item--bold">Pago nomina:</p>
                        <p class="summary__item">
                            <?php if($_POST) {echo "$ ".$pago_nomina;}  else { echo " - - - -"; } ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!--
            Este script sirve para la animacion de los label una vez
            que se detecta que hay escrito algo dentro del innput.
            Mientras se mantenga con texto dentto se les ca a aplicar
            la clase "form__text--is-active".
        -->
        <script src="js/toggleClass.js"></script>
    </body>
</html>
