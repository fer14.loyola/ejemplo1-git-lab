-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-09-2019 a las 23:59:46
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `empleado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datosempleado`
--

CREATE TABLE `datosempleado` (
  `no_nomina` int(5) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `ap_pat` varchar(25) DEFAULT NULL,
  `ap_mat` varchar(25) DEFAULT NULL,
  `calle_no` varchar(50) DEFAULT NULL,
  `colonia` varchar(25) DEFAULT NULL,
  `cp` int(5) DEFAULT NULL,
  `pais` varchar(15) DEFAULT NULL,
  `depto` varchar(25) DEFAULT NULL,
  `rfc` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `datosempleado`
--

INSERT INTO `datosempleado` (`no_nomina`, `nombre`, `ap_pat`, `ap_mat`, `calle_no`, `colonia`, `cp`, `pais`, `depto`, `rfc`) VALUES
(100, 'Rodrigo', 'Gallegos', 'Suárez', 'Cuadrante de san Francisco #26', 'Cuadrante de san Francisc', 4320, 'México', 'Ingeniería', 'GASR901006HDL'),
(200, 'Edgar', 'Salas', 'Lemoine', 'Las flores #1', 'Coyoacan', 3020, 'Mexico', 'Asuntos imp.', 'RESF152010EF1'),
(300, 'Gabriel', 'Ruiz', 'Tellez', 'Villa calamardo', 'Coyoacan', 3020, 'Mexico', 'Ingenieria', 'RE4F152010EF1'),
(400, 'Chevy', 'Mejia', 'Perez', 'Cuadernate de sdfgan', 'Tlalpan', 6423, 'Mexico', 'Ventas', 'DSFH+69H4'),
(500, 'Berny', 'Sandoval', 'Luna', 'OIIOVNI', 'bla bla bla', 5410, 'Mexico', 'Ventas', 'df3g4dsh'),
(600, 'Adriana', 'Vega', 'Palos', 'Reforma #610', 'Cuahutemoc', 4510, 'Mexico', 'Ingenieria', 'DFGG5DGDF4N'),
(700, 'Fernando', 'Aviles', 'LÃ³pez', 'DivisiÃ³n del norte 40', 'Ermita', 12345, 'MÃ©xico', 'Compras', 'avlf801001');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina`
--

CREATE TABLE `nomina` (
  `folio` int(8) NOT NULL,
  `no_nomina` int(5) DEFAULT NULL,
  `salario_diario` decimal(8,2) NOT NULL,
  `no_faltas` int(1) DEFAULT NULL,
  `imss` varchar(16) NOT NULL,
  `isr` decimal(8,2) DEFAULT NULL,
  `fondo_ahorro` decimal(8,2) DEFAULT NULL,
  `vales_despensa` int(4) DEFAULT NULL,
  `ayuda_comedor` int(4) DEFAULT NULL,
  `ayuda_transp` int(4) DEFAULT NULL,
  `salario` decimal(8,2) DEFAULT NULL,
  `calculo_imss` decimal(8,2) DEFAULT NULL,
  `pago_nomina` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nomina`
--

INSERT INTO `nomina` (`folio`, `no_nomina`, `salario_diario`, `no_faltas`, `imss`, `isr`, `fondo_ahorro`, `vales_despensa`, `ayuda_comedor`, `ayuda_transp`, `salario`, `calculo_imss`, `pago_nomina`) VALUES
(2, 100, '800.00', 0, 'GASR-901006DFG1D', '864.00', '240.00', 1024, 700, 900, '12000.00', '504.00', '16232.00'),
(3, 200, '650.00', 2, 'DSAFG6SAF4SADF12', '608.40', '169.00', 1024, 700, 900, '8450.00', '354.90', '12206.30'),
(5, 700, '1000.00', 1, 'g262727', '1008.00', '280.00', 1024, 700, 900, '14000.00', '588.00', '18500.00'),
(6, 400, '1000.00', 1, 'FG6FD6H4FD6G', '1008.00', '280.00', 1024, 700, 900, '14000.00', '588.00', '14748.00'),
(7, 500, '800.00', 2, 'F6DG4FD6H4DFHG', '748.80', '208.00', 1024, 700, 900, '10400.00', '436.80', '11630.40'),
(8, 300, '1000.00', 0, '454g5rg4r5gr4g5', '1080.00', '300.00', 1024, 700, 900, '15000.00', '630.00', '15614.00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `datosempleado`
--
ALTER TABLE `datosempleado`
  ADD PRIMARY KEY (`no_nomina`);

--
-- Indices de la tabla `nomina`
--
ALTER TABLE `nomina`
  ADD PRIMARY KEY (`folio`),
  ADD KEY `no_nomina` (`no_nomina`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `nomina`
--
ALTER TABLE `nomina`
  MODIFY `folio` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `nomina`
--
ALTER TABLE `nomina`
  ADD CONSTRAINT `nomina_ibfk_1` FOREIGN KEY (`no_nomina`) REFERENCES `datosempleado` (`no_nomina`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
