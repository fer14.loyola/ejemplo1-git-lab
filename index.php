<?php
    if($_POST) {
        $nomina = $_POST['nomina'];
        $name = $_POST['name'];
        $ap_pat = $_POST['ap_pat'];
        $ap_mat = $_POST['ap_mat'];
        $direction = $_POST['direction'];
        $colonia = $_POST['colonia'];
        $cp = $_POST['cp'];
        $country = $_POST['country'];
        $depto = $_POST['depto'];
        $rfc = $_POST['rfc'];


        // Conexion con mysql
        $serverName = 'localhost';
        $userName = 'root';
        $password = '';
        $database = 'empleado';
        $field = 'datosempleado';


        $conn = mysqli_connect($serverName, $userName, $password, $database)
            or die ("No se ha podido conectar al servidor de la Base de datos");


        $insert = "INSERT INTO $field
            (no_nomina, nombre, ap_pat, ap_mat, calle_no, colonia, cp, pais, depto, rfc)
            VALUES ($nomina, '$name', '$ap_pat', '$ap_mat', '$direction', '$colonia', $cp, '$country', '$depto', '$rfc')";


        // Validar se el query se ejecuto correctamente
        $resQuery = mysqli_query($conn, $insert);

        if($resQuery) {
            $newRowAdd = 'Nuevo registro añadido exitosamente';
        }
        else {
            $newRowAdd = 'No se ha podido añaadir el registro';
        }








        // conextar con a base de datos para imprimir los datos




        // mysqli_select_db($conn, $database)
        //     or die ("No se ha podido conectar con la Base de datos");

        // $consulta = "SELECT * FROM nomina";


        // $res = mysqli_query($conn, $consulta)
        //     or die ("No se ha podido conectar con la Base de datos");

        // if($res) {
        //     while($row = mysqli_fetch_array($res)) {
        //         echo $row['no_nomina'] . "<br />";
        //         echo $row['imss'] . "<br /> <br />";
        //     }
        // }

        $conn->close();
    }
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/main.css">
        <title>Formulario recibo</title>
    </head>


    <body>
        <div class="wrapper  below">
            <form
                id="send-form"
                method="POST"
                action= "<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>"
                class="form">
                <fieldset>
                    <legend>
                        <h2 class="heading4  title">
                            Datos del empleado
                        </h2>
                    </legend>



                    <div class="form__field">
                        <label for="nomina"
                               class="img  form__icon">
                               <img src="./images/user.svg"
                                    alt="nombre"/>
                        </label>

                        <input type="tel"
                                id="nomina"
                                name="nomina"
                                maxlength="25"
                                class="input  input--small"
                                onchange="changeText(id)"/>

                        <label for="nomina"
                            class="form__text">
                            No. nómina:
                        </label>
                    </div>



                    <div class="form__first-row">
                        <!-- Primera columna -->
                        <div class="form__first-row__col">
                            <div class="form__field">
                                <label for="name"
                                       class="img  form__icon">
                                    <img src="images/user.svg"
                                         alt="nombre"/>
                                </label>

                                <input type="text"
                                        id="name"
                                        name="name"
                                        maxlength="25"
                                        class="input"
                                        onchange="changeText(id)"/>

                                <label for="name"
                                    class="form__text">
                                    Nombre:
                                </label>
                            </div>



                            <div class="form__field">
                                <label for="ap_pat"
                                       class="img  form__icon">
                                    <img src="images/user.svg"
                                         alt="nombre"/>
                                </label>

                                <input type="text"
                                        id="ap_pat"
                                        name="ap_pat"
                                        maxlength="25"
                                        class="input"
                                        onchange="changeText(id)"/>

                                <label for="ap_pat"
                                    class="form__text">
                                    Apellido Paterno:
                                </label>
                            </div>



                            <div class="form__field">
                                <label for="ap_mat"
                                       class="img  form__icon">
                                    <img src="images/user.svg"
                                         alt="nombre"/>
                                </label>

                                <input type="text"
                                        id="ap_mat"
                                        name="ap_mat"
                                        maxlength="25"
                                        class="input"
                                        onchange="changeText(id)"/>

                                <label for="ap_mat"
                                    class="form__text">
                                    Apellido Materno:
                                </label>
                            </div>
                        </div>



                        <!-- Segunda columna -->
                        <div class="form__first-row__col">
                            <div class="form__field">
                                <label for="direction"
                                       class="img  form__icon">
                                    <img src="images/user.svg"
                                         alt="nombre"/>
                                </label>

                                <input type="text"
                                        id="direction"
                                        name="direction"
                                        maxlength="40"
                                        class="input"
                                        onchange="changeText(id)"/>

                                <label for="direction"
                                    class="form__text">
                                    Calle y número:
                                </label>
                            </div>



                            <div class="form__field">
                                <label for="colonia"
                                       class="img  form__icon">
                                    <img src="images/user.svg"
                                         alt="nombre"/>
                                </label>

                                <input type="text"
                                        id="colonia"
                                        name="colonia"
                                        maxlength="40"
                                        class="input"
                                        onchange="changeText(id)"/>

                                <label for="colonia"
                                    class="form__text">
                                    Colonia:
                                </label>
                            </div>



                            <div class="form__field">
                                <label for="cp"
                                       class="img  form__icon">
                                    <img src="images/user.svg"
                                         alt="nombre"/>
                                </label>

                                <input type="tel"
                                        id="cp"
                                        name="cp"
                                        maxlength="40"
                                        class="input"
                                        onchange="changeText(id)"/>

                                <label for="cp"
                                    class="form__text">
                                    Código Postal:
                                </label>
                            </div>
                        </div>
                    </div>



                    <!-- Ultima fila -->
                    <div class="form__last-row">
                        <div class="form__field">
                            <label for="country"
                                   class="img  form__icon">
                                <img src="images/user.svg"
                                     alt="nombre"/>
                            </label>

                            <input type="text"
                                    id="country"
                                    name="country"
                                    maxlength="40"
                                    class="input  input--mr"
                                    onchange="changeText(id)"/>

                            <label for="country"
                                class="form__text">
                                País:
                            </label>
                        </div>



                        <div class="form__field">
                            <label for="depto"
                                   class="img  form__icon">
                                <img src="images/user.svg"
                                     alt="nombre"/>
                            </label>

                            <input type="text"
                                    id="depto"
                                    name="depto"
                                    maxlength="40"
                                    class="input  input--mr"
                                    onchange="changeText(id)"/>

                            <label for="depto"
                                class="form__text">
                                Departamento:
                            </label>
                        </div>



                        <div class="form__field">
                            <label for="rfc"
                                   class="img  form__icon">
                                <img src="images/user.svg"
                                     alt="nombre"/>
                            </label>

                            <input type="text"
                                    id="rfc"
                                    name="rfc"
                                    maxlength="40"
                                    class="input"
                                    onchange="changeText(id)"/>

                            <label for="rfc"
                                class="form__text">
                                RFC:
                            </label>
                        </div>
                    </div>



                    <!-- Boton de enviar -->
                    <div class="form__buttton-send">
                        <input
                            type="submit"
                            name="send"
                            value="Enviar"
                            class="btn  btn--large">
                    </div>
                </fieldset>


                <a href="form.php" class="form__btn-color">
                    <div class="img  form__next">
                        <img src="./images/next.svg" alt="">
                    </div>
                </a>

                <!-- Validacion de los registros -->
                <h3 class="heading4  subtitle  form__valid">
                    <?php if($_POST) {echo $newRowAdd;} ?>
                </h3>
            </form>

        </div>

        <!--
            Este script sirve para la animacion de los label una vez
            que se detecta que hay escrito algo dentro del innput.
            Mientras se mantenga con texto dentto se les ca a aplicar
            la clase "form__text--is-active".
        -->
        <script src="js/toggleClass.js"></script>
    </body>
</html>
