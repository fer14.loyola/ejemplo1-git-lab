/**
 * Este script aplica estilos a un <label> cuando su <input> contiene texto
 * Si se agregan más campos, se almacenan en las variables
*/



/*
  Se van a buscar todos los elementos con las clases .input y
  .form__text, y se van a guardar en sus respectivas variables
*/
const input = document.querySelectorAll('.input');
const label = document.querySelectorAll('.form__text');

// La función recibe el "id" del <input> que activa onchange=""
function changeText(e) {

  // Se recorre el arreglo de todos los elementos <input>
  for(let i = 0; i < input.length; i++) {
    // Se accede al "id" del <input>
    const id = input[i].id

    /**
     * Se compara el "id" de cada <input> con el que recibe la función
     * Si el "id" coincide, se sabe que <input> llamó a la función
    */
    if(e === id) {
      // Se accede al valor que contiene el <input> y se almacena
      const value = input[i].value
      // Se accede al <label> que corresponde al <input> que llamó a la función
      const valueLabel = label[i]


      /**
       * Se varifica que el valor del <input> contenga texto
       * Si tiene texto se le agrega esa clase a su <label>
      */
      if(value != "") {
         valueLabel.classList.add('form__text--is-active');
      } else {
         valueLabel.classList.remove('form__text--is-active');
      }
    }
  }
}

